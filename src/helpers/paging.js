const paging = (totalRecord, currentPage, limit) => {
  try {
    let totalPage = Math.ceil(totalRecord/limit)

    if(currentPage > totalPage) {
      currentPage = totalPage
    } else if (currentPage < 1) {
      currentPage = 1
    }
  
    const start = (currentPage - 1) + limit
  
    let html = '<ul class="pagination justify-content-center text-center">'
  
    if (currentPage > 1 && totalPage > 1) {
      html += '<li class="page-item"><a class="page-link text-dark h-100" href="/admin/products?page='+(currentPage-1)+'"><i class="fas fa-chevron-left"></i></a></li>'
    }
    else {
      html += '<li class="page-item disabled"><a class="page-link text-secondary h-100 " href=""><i class="fas fa-chevron-left"></i></a></li>';
    }
  
    for (let index = 1; index <= totalPage ; index ++) {
      if (index == currentPage){
        html += '<li style="width: 35px;" class="page-link bg-dark text-white">'+index+'</li>';
      }
      else{
        html += '<li style="width: 35px;" class="page-item "><a class="page-link text-dark" href="/admin/products?page='+index+'">'+index+'</a></li>';
          }
    }
  
    if (currentPage < totalPage && totalPage > 1){
      html += '<li class="page-item"><a class="page-link  h-100 text-dark" href="/admin/products?page='+(currentPage+1)+'"><i class="fas fa-chevron-right"></i></a></li></ul>';
    }
    else {
      html += '<li class="page-item disabled"><a class="page-link text-secondary h-100" href=""><i class="fas fa-chevron-right"></i></a></li></ul>';
    }
    
    return [
      start,
      limit,
      html,
    ] 
  } catch (error) {
    console.log(error)
  }

}

module.exports = paging