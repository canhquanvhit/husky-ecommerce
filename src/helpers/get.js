const convertToObject = (obj) => {
    return obj.toObject()
}

const getUser = (req) => {
    let user
    if(req.user)
    {
        user = convertToObject(req.user)
    }
    return user
}
module.exports = {convertToObject, getUser}