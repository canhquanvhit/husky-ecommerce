const express = require('express');
const router = express.Router();
const {checkAdminMiddleware} = require('../middleware/checkAdmin.middleware')

const AdminSiteController = require('../app/controllers/admin.controller');
const ProductsController = require('../app/controllers/products.controller');
const productsRouter = require('./products.router')

router.get('/login', AdminSiteController.getLoginPage)
router.post('/login', AdminSiteController.login)
router.use('/search',checkAdminMiddleware, AdminSiteController.search);
router.use('/', checkAdminMiddleware, AdminSiteController.index);

module.exports = router;
