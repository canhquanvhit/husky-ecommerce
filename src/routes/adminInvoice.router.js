const express = require('express');
const router = express.Router();
const invoicesController = require('../app/controllers/invoices.controller');
const authClientMiddleware = require('../middleware/authClient.middleware');

router.put('/:id', invoicesController.handleEdit)
router.get('/:id', invoicesController.edit)
router.post('/', invoicesController.show)
router.use('/', invoicesController.show)

module.exports = router
