const productsRouter = require('./products.router')
const siteRouter = require('./site.router')
const adminRouter = require('./admin.router')
const accountRouter = require('./account.router')
const invoiceRouter = require('./invoices.router')
const adminInvoiceRouter = require('./adminInvoice.router')
const categoryRouter = require('./category.router')
const subCategoryRouter = require('./subCategory.router')
const clientProductRouter = require('./clientProduct.router')
const provinceApi = require('./province.handler')
function route(app) {
  app.use('/provinceAPIs', provinceApi)
  app.use('/products', clientProductRouter)
  app.use('/checkout', invoiceRouter)
  app.use('/admin/invoices', adminInvoiceRouter)
  app.use('/admin/subcategories', subCategoryRouter)
  app.use('/admin/categories', categoryRouter)
  app.use('/admin/products', productsRouter)
  app.use('/admin', adminRouter)
  app.use('/account', accountRouter)
  app.use('/', siteRouter)

}
module.exports = route;
