const express = require('express');
const router = express.Router();
const upload = require('../middleware/upload.middleware')

const productsController = require('../app/controllers/products.controller');
const productDetailController = require('../app/controllers/productDetail.controller');

const cpUpload = upload.fields([{ name: 'avatar', maxCount: 1 }, { name: 'moreImages', maxCount: 4 }])


router.get('/add', productsController.viewAdd) 
router.use('/subCategory',productsController.sendSubCategory)
router.get('/:id/edit', productsController.edit)
router.put('/:id/edit', productsController.updateOne)
router.use('/:id/delete', productsController.deleteOne)
router.post('/:id/detail/add', productDetailController.createOne)
router.get('/:id/detail', productDetailController.show)
router.post('/add', cpUpload, productsController.createOneProduct)

router.use('/', productsController.show)

module.exports = router;
