const express = require('express');
const router = express.Router();
const request = require('request')

router.get('/provinces', async (req, res) => {
  try {
    request.get('https://thongtindoanhnghiep.co/api/city', (error, response, body) => {
      return res.status(200).json(JSON.parse(body))
    })
  } catch (error) {
    return res.status(400).json(error)
  }
})

router.get('/provinces/:id/district', async (req, res) => {
  try {

    const id = req.params.id
    request.get(`https://thongtindoanhnghiep.co/api/city/${id}/district`, (error, response, body) => {
      return res.status(200).json(JSON.parse(body))
    })
  } catch (error) {
    return res.status(400).json(error)
  }
})

router.get('/district/:id/ward', async (req, res) => {
  try {

    const id = req.params.id
    request.get(`https://thongtindoanhnghiep.co/api/district/${id}/ward`, (error, response, body) => {
      return res.status(200).json(JSON.parse(body))
    })
  } catch (error) {
    return res.status(400).json(error)
  }
})

module.exports = router