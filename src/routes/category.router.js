const express = require('express');
const categoriesController = require('../app/controllers/categories.controller');
const router = express.Router();
const CategoriesController = require('../app/controllers/categories.controller')
// router.use('/add', CategoriesController.myAddress)
router.get('/:id/edit', categoriesController.edit)
router.put('/:id/edit', categoriesController.updateOne)
router.delete('/:id/delete', categoriesController.deleteOne)
router.post('/add', categoriesController.createOneCat)
router.use('/add', categoriesController.viewAdd)
router.use('/', CategoriesController.show)


module.exports = router
