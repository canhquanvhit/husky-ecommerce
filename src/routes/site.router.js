const express = require('express');
const router = express.Router();

const siteController = require('../app/controllers/site.controller');
const checkUserLogged = require('../middleware/checkUserLoged.middleware');

router.use('/search',checkUserLogged, siteController.search);
router.use('/', checkUserLogged, siteController.show);
module.exports = router;
