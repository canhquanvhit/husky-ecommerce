const express = require('express');
const router = express.Router();

const productDetailController = require('../app/controllers/productDetail.controller');
const checkUserLogged = require('../middleware/checkUserLoged.middleware');
const productsController = require('../app/controllers/products.controller')

router.get('/productBySub/:id', productsController.productsBySubCat)
router.get('/productByCat/:id', productsController.productsByCat)
router.get('/getSizeByColor', productDetailController.getSizeByProductIdAndColor)
router.get('/:id',checkUserLogged, productDetailController.getAllDetailsByProductID)

module.exports = router;
