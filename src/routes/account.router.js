const express = require('express');
const passport = require('passport')
const router = express.Router();
const accountController = require('../app/controllers/account.controller')
const authClientMiddleware = require('../middleware/authClient.middleware')
router.use('/address',authClientMiddleware, accountController.myAddress)

router.get('/orders/:id', accountController.myOrder)
router.post('/login', accountController.login)
router.use('/login', accountController.getLogin)
router.use("/logout", accountController.logout)
router.post('/register', accountController.reqRegisterUser)
router.get('/register', accountController.registerUser)
router.get('/addresses', authClientMiddleware, accountController.myAddress)
router.get('/',authClientMiddleware, accountController.myAccount)
module.exports = router
