const express = require('express');
const subCategoryController = require('../app/controllers/subCategories.controller');
const router = express.Router();

router.put('/:id/edit', subCategoryController.updateOne)
router.get('/:id/edit', subCategoryController.showEditView)
router.post('/add', subCategoryController.createOneSubCat)
router.get('/add', subCategoryController.showAddView)
router.use('/', subCategoryController.show)


module.exports = router
