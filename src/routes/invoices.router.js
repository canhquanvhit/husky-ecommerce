const express = require('express');
const router = express.Router();
const invoicesController = require('../app/controllers/invoices.controller');
const authClientMiddleware = require('../middleware/authClient.middleware');

router.post('/', invoicesController.handleCheckout)
router.use('/', authClientMiddleware, invoicesController.checkout)

module.exports = router
