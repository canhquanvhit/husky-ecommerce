const Joi = require('joi');

const authSchema = Joi.object({
    lastName: Joi.string()
        .trim()
        .min(3)
        .max(30)
        .required(),
    firstName: Joi.string()
        .trim()
        .min(3)
        .max(30)
        .required(),
    birthday: Joi.date()
        .required,
    password: Joi.string()
        .required()
        .min(8)
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

    repeat_password: Joi.ref('password'),

    email: Joi.string()
        .trim()
        .lowercase()    
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'vn'] } })
})
    .with('username', 'birth_year')
    .xor('password', 'access_token')
    .with('password', 'repeat_password');

module.exports  = { authSchema }