const e = require('express')
const { query } = require('express')
const productDetailsModel = require('../models/productDetails.model')
const { distinct, findOneAndUpdate } = require('../models/productDetails.model')
const productDetailModel = require('../models/productDetails.model')
const colorsService = require('./colors.service')
const subCategoriesService = require('./subCategories.service')

class ProductDetailsService {
  async findMany (query) {
    try {
      const productDetails = await productDetailsModel
        .find(query)                                             
        .populate({
          path: 'size'
        })
        .populate({
          path: 'color'
        })
        .exec() 

      if(!productDetails){
        return null
      }

      const result = productDetails.map(productDetail => productDetail.toObject())
      return result

      } catch (error) {
    }
  }

  async findManyWithDistinct(query) {
    const productDetails = await productDetailsModel
      .find(query)                                      
      .distinct('color')
  
    if(!productDetails) {
      return null
    }

    const colors = []
    for (const element of productDetails) {
      colors.push(await colorsService.findOne(element))
    }

    return colors
  }

  async findSizeByColor(query) {
    try {
      const productDetails = await productDetailsModel
      .find(query)
      .populate({
        path: 'size'
      })

      if(!productDetails) {
        return null
      }

      const result = productDetails.map(productDetail => productDetail.toObject())
    return  result
    } catch (error) {
      console.log(error)
    }

  }

  async updateInStock(id, quantity, method) {
    try {
      const productDetail = await this.findOne(id)
      const { inStock: preInStock} = productDetail

      let inStock
      if (method == 'export') {
        inStock = preInStock - quantity
      }
      
      if (method == 'import') {
        inStock = preInStock + quantity
      }

      await findOneAndUpdate({_id: id}, {inStock: inStock})
    } catch (error) {
      
    }
  }
  async createOne (data) {
    try {
      const productDetail = new productDetailModel(data)
      await productDetail.save()
      return productDetail.toObject()
      
    } catch (error) {
      console.log(error);
    }
  }

  async findOne (id) {
    try {
      const productDetail = await productDetailModel.findOne({_id: id})
      .populate({
        path: 'product'
      })
      .populate({
        path: 'size'
      })
      .populate({
        path: 'color'
      })

      return productDetail.toObject()
    } catch (error) {
      
    }
  }

  async findOneByColorAndSize (color, size) {
    try {
      const productDetail = await productDetailModel.findOne({color, size})
      return productDetail.toObject()
    } catch (error) {
      
    }
  }
}

module.exports = new ProductDetailsService()