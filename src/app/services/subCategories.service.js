const categoriesModel = require('../models/categories.model')
const subCategoriesModel = require('../models/subCategories.model')

class subCategoriesService {
	async createOne (data)  {
  try {
    const subCategory = new subCategoriesModel(data)
    await subCategory.save()
    return subCategory
    
  } catch (error) {
    console.log(error)
  }
}

  async findOne (id) {
    try {
      const subCategory = await subCategoriesModel
        .findOne({_id: id})
        .populate('category')

      if(!subCategory){
        return null
      }
  
      return subCategory.toObject()

    } catch (error) {

    }
  }

  async findMany  (query) {
    try {
      const subCategories = await subCategoriesModel
        .find(query)                                              
        .populate({
          path: 'category',
        })
        .exec() 

      if(!subCategories){
        return null
      }
      const result = subCategories.map(subCategory => subCategory.toObject())
      return result

    } catch (error) {
      
    }
  }

  async deleteOne(id) {
    try {
      const subCategory = await subCategoriesModel.findById(id).exec()

      if(!subCategory){
        return "SubCategoryNotFound"
      }

      return await subCategoriesModel.deleteOne({_id: id})

    } catch (error) {
      
    }
  }

  async updateOne (id, updateOneData) {
    try {
      const subCategory =  await subCategoriesModel.updateOne({_id: id}, updateOneData)
      return subCategory
    } catch (error) {
      
    }
  }

  async checkExist (name) {
    try {
      const subCategory = await categoriesModel.findOne({name}).exec()
      if (subCategory)
        return true

      return false
    } catch (error) {
      
    }
  }
}

module.exports = new subCategoriesService()