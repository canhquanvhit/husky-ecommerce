const invoiceDetailModels = require('../models/invoiceDetails.model')

class InvoiceDetailService {
  async findMany(query) {
    try {
      const invoiceDetail = await invoiceDetailModels.find(query)
        .sort({createdAt: -1})
      return invoiceDetail.map(size => size.toObject())
    } catch (error) {
      
    }
  }


  async createOne(data) {
    try {
      const invoiceDetail = new invoiceDetailModels(data)
      await invoiceDetail.save()
      return invoiceDetail.toObject()
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = new InvoiceDetailService()