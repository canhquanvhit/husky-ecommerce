const addressModels = require('../models/address.model')

class AddressService {
  async findMany(query) {
    try {
      const addresses = await addressModels.find(query)
      return addresses.map(size => size.toObject())
    } catch (error) {
      
    }
  }

  async createOne(data) {
    try {
      const address = new addressModels(data)
      await address.save()
      return address.toObject()
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = new AddressService()