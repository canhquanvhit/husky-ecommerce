const sizesModel = require('../models/sizes.model')
class SizesService {
  async findMany(query) {
    try {
      const sizes = await sizesModel.find(query)
      return sizes.map(size => size.toObject())
    } catch (error) {
      
    }
  }

  async createOne(data) {
    try {
      const size = new sizesModel(data)
      await size.save()
      return size.toObject()
    } catch (error) {
      console.log(error)
    }
  }

  async checkExist(name) {
    try {
      const size = await sizesModel.findOne({name: name})
      if (size) {
        return true
      }

      return false
    } catch (error) {
      console.log(error)
    }
  }

  async findOne(name) {
    try {
      const size = await sizesModel.findOne({name: name})
      if (size) {
        return size.toObject()
      }

      return null
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = new SizesService()