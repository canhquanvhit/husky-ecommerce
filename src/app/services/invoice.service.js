const invoiceModels = require('../models/invoices.model')

class InvoiceService {
  async findMany(query) {
    try {
      const invoices = await invoiceModels
        .find(query)
        .populate({
          path: 'user',
        })
        .sort({createdAt: -1})
        .exec() 
      return invoices.map(size => size.toObject())
    } catch (error) {
      
    }
  }

  async findOne(id) {
    try {
      const invoice = await invoiceModels
        .findOne({_id: id})
        .populate({
          path: 'user',
        })
        .populate({
          path: 'user',
        })
        .exec() 
      return invoice.toObject()
    } catch (error) {
      
    }
  }

  async createOne(data) {
    try {
      const invoice = new invoiceModels(data)
      await invoice.save()
      return invoice.toObject()
    } catch (error) {
      console.log(error)
    }
  }

  async updateOne(id, data) {
    try {
      const invoice = await invoiceModels
        .findOneAndUpdate({_id: id}, {status: data.status})
      return invoice.toObject()
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = new InvoiceService()