const { required } = require('joi')
const usersModel = require('../models/users.model')

class UsersService {
    async createOne(userInput) {
        try {
            const user = new usersModel(userInput)
            await user.save()
            return user
            
        } catch (error) {
            return Promise.reject(error)
        }
    }
    
    async checkEmailExist(email) {
        try {
            const user = await usersModel.findOne({email}).exec()
            if (user)
                return user
    
            return null
        } catch (error) {
            return Promise.reject(error)
        }
    }

    async findOne(query) {
        try {
            const user = await usersModel.findOne(query)

            if (!user) {
                throw {
                    code: 404,
                    name: 'UserNotFound'
                }
            }

            return user
        } catch (error) {
            return Promise.reject(error)
        }
    }
}

module.exports.UserService =  UsersService