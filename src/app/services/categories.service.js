const categoriesModel = require('../models/categories.model')
const subCategoriesService = require('./subCategories.service')

class categoriesService {
	async createOne (data)  {
  try {
    const category = new categoriesModel(data)
    await category.save()
    return category
    
  } catch (error) {
    console.log(error);
  }
}

  async findOne (id) {
    try {
      const category = await categoriesModel.findOne({_id: id})
        .populate({
          path: 'category',
        })
        .exec()

      if(!category){
        return null
      }
  
      return category.toObject()

    } catch (error) {

    }
  }

  async findMany  () {
    try {
      const categories = await categoriesModel.find()

      if(!categories){
        return null
      }
      const result = categories.map(category => category.toObject())
      return result

    } catch (error) {
      
    }
  }

  async findRelated(category) {
    const subCategories =  await subCategoriesService.findMany({category: category._id})
        const newCate = {
          ...category,
          subCategories,
        }
        return newCate
  }

  async findManyIncludesSub() {
    try {
      let categories = await categoriesModel.find()

      if(!categories){
        return null
      }
      categories = categories.map(category => category.toObject())
      const newCategories = []
      for (const category of categories) {
        const newCategory = await this.findRelated(category)
        newCategories.push(newCategory)
      }
      return newCategories

    } catch (error) {
      
    }
  }

  async deleteOne(id) {
    try {
      const category = await categoriesModel.findById(id).exec()

      if(!category){
        return "CategoryNotFound"
      }

      return await categoriesModel.deleteOne({_id: id})

    } catch (error) {
      
    }
  }

  async updateOne (id, updateOneData) {
    try {
      const category =  await categoriesModel.updateOne({_id: id}, updateOneData)
      return category
    } catch (error) {
      
    }
  }

  async checkExist (name) {
    try {
      const category = await categoriesModel.findOne({name}).exec()
      if (category)
        return true

      return false
    } catch (error) {
      
    }
  }
}

module.exports = new categoriesService()