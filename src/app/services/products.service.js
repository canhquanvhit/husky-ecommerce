const paging = require('../../helpers/paging')
const productDetailsModel = require('../models/productDetails.model')
const productModels = require('../models/products.model')
const subCategoriesService = require('./subCategories.service')
class ProductsService {
  async findMany (query, limit, skip, currentPage) {
    try {
      const products = await productModels
        .find(query)  
        .limit(limit)
        .skip(skip)                                            
        .populate({
          path: 'subCategory',
        })
        .sort({createdAt: 'desc'})
        .exec() 

      if(!products){
        return null
      }
      
      const totalRecord = await productModels.count()

      const pagination = paging(totalRecord, currentPage, limit)

      let result = products.map(product => product.toObject())

      result.pagination = pagination
      return result

      } catch (error) {
    }
  }
  async findRandom (query, limit, skip, currentPage) {
    try {
      const products = await productModels
        .find(query)  
        .limit(limit)
        .skip(skip)                                            
        .populate({
          path: 'subCategory',
        })
        .sort({createdAt: 'asc'})
        .exec() 

      if(!products){
        return null
      }
      
      const totalRecord = await productModels.count()

      const pagination = paging(totalRecord, currentPage, limit)

      let result = products.map(product => product.toObject())

      result.pagination = pagination
      return result

      } catch (error) {
    }
  }

  async createOne (data) {
    try {
      const product = new productModels(data)
      await product.save()
      return product
      
    } catch (error) {
      console.log(error);
    }
  }

  async findOne (id) {
    try {
      const product = await productModels.findOne({_id: id})
      return product.toObject()
    } catch (error) {
      
    }
  }

  async findLatestProduct() {
    const products = await productModels
    .find()                                              
    .populate({
      path: 'subCategory',
    })
    .limit(10)
    .sort({createdAt: -1})
    .exec() 

  if(!products){
    return null
  }
  
  const result = products.map(product => product.toObject())
  return result

  }

  async findBySubCategory(id) {
    const products = await productModels
    .find({subCategory: id})
    .populate({
      path: 'subCategory',
    })

    if(!products) {
      return null
    }

    const productsToObject = products.map(product => product.toObject())

    return productsToObject
  }

  async deleteOne(id) {
    try {
      const product = await productModels.findById(id).exec()

      if(!product){
        return "productNotFound"
      }
      await productDetailsModel.deleteMany({product: id})
      await productModels.deleteOne({_id: id})
      return true
    } catch (error) {
      return false
    }
  }
}

module.exports = new ProductsService()