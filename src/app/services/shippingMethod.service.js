const shippingMethodModels = require('../models/shippingMethods.model')

class ShippingMethodService {

  async findMany(query) {
    try {
      let colors = await shippingMethodModels.find(query)
      
      colors = colors.map(color => color.toObject())

      return colors
    } catch (error) {
      
    }
  }

  async createOne(data) {
    try {
      const shippingMethod = new shippingMethodModels(data)
      shippingMethod.save()
      return shippingMethod.toObject()
    } catch (error) {
      console.log(error)
    }
  }

  async findOne() {
    try {
      
      const data = {
        transporter: 'Giao hàng tiết kiệm',
      } 

      const shippingMethod = await shippingMethodModels.findOne({transporter: 'Giao hàng tiết kiệm'})
      shippingMethod.feeShip = shippingMethod.feeShip
      return shippingMethod.toObject()
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = new ShippingMethodService()