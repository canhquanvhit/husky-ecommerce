const colorsModel = require('../models/colors.model')
const ColorsModel = require('../models/colors.model')
class ColorsService {
  async createOne(data) {
    try {
      const color = new ColorsModel(data)
      color.save()
      return color.toObject()
    } catch (error) {
      console.log(error)
    }
  }

  async findMany(query) {
    try {
      let colors = await colorsModel.find(query)
      
      colors = colors.map(color => color.toObject())

      return colors
    } catch (error) {
      
    }
  }

  async findOneByName(name) {
    try {
      const color = await colorsModel.findOne({name: name})

      return color.toObject()
    } catch (error) {
      
    }
  }

  async findOne(id) {
    try {
      const color = await colorsModel.findOne(id)

      return color.toObject()
    } catch (error) {
      
    }
  }
}

module.exports = new ColorsService()