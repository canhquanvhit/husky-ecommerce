const categoriesService = require('../services/categories.service')


class CategoriesController {

    async show(req, res) {
        const categories = await categoriesService.findMany()
        return res.render('admin/categories/list', {
            layout: 'admin/categories',
            categories,
        })
    }

    async viewAdd(req, res) {
        return res.render('admin/categories/add', {layout: 'admin/categories'})
    }

    async edit(req, res) {
        const category = await categoriesService.findOne(req.params.id)

        return res.render('admin/categories/edit', {
            layout: 'admin/sub',
            category })
    }

    async createOneCat(req, res) {
        try {
            const data = req.body
            const { name } = data
            const checking = await categoriesService.checkExist(name)
            if(checking) {
                res.render('admin/categories/add', {
                    layout: 'admin/categories',
                    nameError: true})
            }

            const category = await categoriesService.createOne(data)
            if (category) {
                res.render('admin/categories/add', {
                    layout: 'admin/categories',
                    message: 'thành công!'})
            }
        } catch (error) {
            console.log(error)
        }
    }

    // PUT /:id/edit
    async updateOne(req, res) {
        const updateData = req.body
        await categoriesService.updateOne(req.params.id, updateData)

        return res.redirect('/admin/categories')
    }

    // DELETE /:id/delete
    async deleteOne(req, res) {
        const { id } = req.params

        await categoriesService.deleteOne(id)
        
        res.redirect('/admin/categories')
        
    }
}

module.exports = new CategoriesController();
