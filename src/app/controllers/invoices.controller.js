
const {getUser} = require('../../helpers/get')
const addressService = require('../services/address.service')
const invoiceService = require('../services/invoice.service')
const invoiceDetailService = require('../services/invoiceDetail.service')
const productsService = require('../services/products.service')
const productsDetailService = require('../services/productsDetail.service')
const shippingMethodService = require('../services/shippingMethod.service')
class invoicesController {
    async checkout (req, res, next) {
        let { error: stringError, result} = req.query
        const error = {
            name: false,
            phone: false,
            address: false,
        }

        if(stringError) {
            const arrayError = stringError.split(',')
            if(arrayError) {
                if (arrayError[0]  == 'true')
                {
                    error.name  = true
                }
        
                if (arrayError[1]  == 'true')
                {
                    error.phone  = true
                }
        
                if (arrayError[2] == 'true')
                {
                    error.address  = true
                }      
            }
        }        
       
        const user = getUser(req)
        const shippingMethod = await shippingMethodService.findOne()
        res.render('client/checkout', {
            layout: 'checkout',
            user,
            shippingMethod,
            error,
            result,
        })
    }

    async handleCheckout (req, res, next) {
        const data = req.body
        const { user, fullName, phoneNumber,
                addressDetail, totalAmount, productDetails : ProductDetailsAsString,
                province, district, subDistrict,
                paymentMethod,
                shippingMethod,
                }  = data
        const error = {
            fullName:false,
            phoneNumber: false,
            addressDetail: false,
        }

        if(!fullName) {
            error.fullName = true
        }

        if(!phoneNumber) {
            error.phoneNumber = true
        }

        if(!addressDetail) {
            error.addressDetail = true
        }

        if(error.fullName == true || error.phoneNumber || error.addressDetail) {
            return res.redirect(`/checkout?error=${error.fullName},${error.phoneNumber},${error.addressDetail}`)
        }
        /// thêm địa chỉ
        const addressData = {
            user,
            fullName,
            phoneNumber,
            province,
            district,
            subDistrict,
            addressDetail,
        }
        
        await addressService.createOne(addressData)
        
        const shipmentInfo = 'họ tên:' +fullName + ', ' + '<br>' + 'SĐT:' + phoneNumber +
                             '<br>Địa chỉ: ' + addressDetail + ', ' + subDistrict + ', ' + district + ', ' + province 
        const invoiceData = {
            user,
            paymentMethod,
            shipmentInfo,
            shippingMethod,
            totalAmount,
        }
        /// thêm hóa đơn
        const invoice =     await invoiceService.createOne(invoiceData)

        const productDetails = JSON.parse(ProductDetailsAsString.replace(/'/g, '"'))

        for(const element of productDetails) {
            const { color, size, amount } = element
            const productDetail = await productsDetailService.findOneByColorAndSize(color, size)
            
            const invoiceDetailData = {
                invoice: invoice._id,
                productDetail: productDetail._id,
                quantity: amount,
            }

            await invoiceDetailService.createOne(invoiceDetailData)
            
            // update inStock
            await productsDetailService.updateInStock(productDetail._id, amount, 'export')
        }

        return res.redirect(`/checkout?result=done`)
    }

    async show (req, res, next) {
        const invoices = await invoiceService.findMany()
        res.render('admin/invoices/list', {
            layout: 'admin/home',
            invoices,
        })
    }

    async edit (req, res, next) {
        const id = req.params.id

        const invoice = await invoiceService.findOne(id)
        let invoiceDetails = await invoiceDetailService.findMany({invoice: id})
        for(const invoiceDetail of invoiceDetails) {
            const productDetail = await productsDetailService.findOne(invoiceDetail.productDetail._id)
            invoiceDetail.productDetail = productDetail
        }
        res.render('admin/invoices/edit', {
            layout: 'admin/sub',
            invoice,
            invoiceDetails
        })
    }

    async handleEdit(req, res) {
        const id = req.params.id

        const {status} = req.body
        const updateData = {
            status,
        }

        await invoiceService.updateOne(id, updateData)

        const invoices = await invoiceService.findMany()
        res.render('admin/invoices/list', {
            layout: 'admin/sub',
            invoices,
        })
    }
}

module.exports = new invoicesController()