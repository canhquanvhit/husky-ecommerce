const { convertToObject } = require('../../helpers/get');
const categoriesService = require('../services/categories.service');
const productsService = require('../services/products.service');
const subCategoriesService = require('../services/subCategories.service')
const {getUser} = require('../../helpers/get')
class SiteController {
    // home
    async show(req, res) {
        try {
            const user = getUser(req)
            const categories = await categoriesService.findManyIncludesSub()
            const latestProducts = await productsService.findLatestProduct()
            const products = await productsService.findRandom(null, 10)
            res.render('client/home', {
                layout: 'main',
                categories,
                latestProducts,
                products,
                user,
                })
            
        } catch (error) {
            console.log(error)
        }
        }

    search(req, res) {
        res.send('client/search');
    }

}

module.exports = new SiteController();
