const productsService = require("../services/products.service")
const sizesService = require("../services/sizes.service")
const colorsService = require('../services/colors.service')
const productDetailService = require('../services/productsDetail.service')
const productsDetailService = require("../services/productsDetail.service")
const { getUser } = require("../../helpers/get")
const categoriesService = require("../services/categories.service")

class ProductDetailController {

  async getAllDetailsByProductID(req, res) {
    const user = getUser(req)
    const id = req.params.id
    const product = await productsService.findOne(id)

    product.priceFormatted = product.price.toLocaleString('vi', {style : 'currency', currency : 'VND'})
    const colors = await productDetailService.findManyWithDistinct({product: id})
    const categories = await categoriesService.findManyIncludesSub()
    return res.render('client/productDetail', {
      layout: 'main',
      product,
      colors,
      user,
      categories,
    })
  }
  
  async getSizeByProductIdAndColor(req, res) {
    const productID = req.query.product
    const colorID = req.query.color
  
    const query = {
      product: productID,
      color: colorID,
    }

    const sizes = await  productsDetailService.findSizeByColor(query)

    
    return res.render('client/getSizeByColor', {
      layout: false,
      sizes,
    })
  }

  async show (req, res) {
    const user = getUser(req)
    const { id } = req.params
    const product = await productsService.findOne(id)
    const sizes = await sizesService.findMany()
    const colors = await colorsService.findMany()
    const productDetails = await productsDetailService.findMany({product: product })
    res.render('admin/products/detail',{
      layout: 'admin/sub',
      product,
      sizes,
      colors,
      productDetails,
      user,
    })
  }

  async createOne(req, res) {
    const data = req.body

    const product = req.params.id
    let {newSize, newColor, inStock, size, color, newInStock } = data

    if (newSize && newColor) {
      size = await sizesService.findOne(newSize)

      if(!size) {
        size =  await sizesService.createOne({name: newSize})
      }
    
      color = await colorsService.findOneByName(newColor)
      
      if(!color) {
        color = await colorsService.createOne({name: newColor})

      }

      await productDetailService.createOne({
        size: size._id,
        color: color._id,
        product,
        inStock: newInStock,
      })

    }

    if (!newSize || !newColor){
      await productDetailService.createOne({
        size,
        color,
        product,
        inStock,
      })
    }
    res.redirect('/admin/products/'+product+'/detail')
  }

}

module.exports = new ProductDetailController()