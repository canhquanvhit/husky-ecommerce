const productsService = require('../services/products.service')
const subCategoriesService = require('../services/subCategories.service')
const categoriesService = require('../services/categories.service')
const upload = require('../../middleware/upload.middleware');
const Resize = require('../../helpers/resize');
const { fields } = require('../../middleware/upload.middleware');
const { json } = require('body-parser');
const imagePath = '/home/huskywanafly/husky-ecommerce/src/public/images'
const fileUpload = new Resize(imagePath)
class ProductsController {

  async show(req, res) {
    const currentPage = Number(req.query.page) || 1
    const limit = 5
    const skip = (currentPage * limit - limit)
    const products = await productsService.findMany(null, limit, skip, currentPage)
    return res.render('admin/products/list', {
      layout: 'admin/categories',
      products,
    })
  }

  async viewAdd(req, res) {
    const categories = await categoriesService.findMany()
    return res.render('admin/products/add', {
      layout: 'admin/categories',
      categories,
    })
  }

  async sendSubCategory(req, res) {
    const categoryID = req.query.catid
    const subcategories =   await subCategoriesService.findMany({category: categoryID})
    return res.render('admin/products/getSubByCatID', {
      layout: false,
      subcategories,
    })
  }

  async edit(req, res) {
    const category = await categoriesService.findOne(req.params.id)

    return res.render('admin/categories/edit', {
      layout: 'admin/sub',
      category })
  }

  async createOneProduct(req, res) {
    try {
      const {avatar, moreImages } = req.files
      const data = req.body

      const avatarFileName = await fileUpload.save(avatar[0].buffer)

      const fileNames = []
      if(moreImages) {
        for (const image of moreImages) {
          fileNames.push( await fileUpload.save(image.buffer))
        }

      }

      const createOneData = {
        ...data,
        avatar: avatarFileName,
        moreImages: fileNames || null,
      }

      await productsService.createOne(createOneData)

      res.redirect('/admin/products')
    } catch (error) {
      res.send(json({error: "failure"}))
    }
  }

  // PUT /:id/edit
  async updateOne(req, res) {
    const updateData = req.body
    await categoriesService.updateOne(req.params.id, updateData)

    return res.redirect('/admin/categories')
  }

  // DELETE /:id/delete
  async deleteOne(req, res) {
    const { id } = req.params

    const result = await productsService.deleteOne(id)
    
    if(result) {
      res.redirect('/admin/products')
    }
  
  }
  
  async productsByCat(req, res) {
    const categoryID = req.params.id
    const categories = await categoriesService.findManyIncludesSub()
    const category = await categoriesService.findOne(categoryID)
    const subCategories = await subCategoriesService.findMany({category: categoryID})

    const products = []

    for(const subCategory of subCategories) {
      const productBySub = await productsService.findBySubCategory(subCategory._id)
      if(productBySub) {
        products.push(...productBySub)
      }
    }

    return res.render('client/productByCate',
                    {layout: 'main',
                    categories,
                     products,
                     category})
  }

  async productsBySubCat(req,res) {
    const subCategoryID = req.params.id
    
    const subCategory = await subCategoriesService.findOne(subCategoryID)
    const categories = await categoriesService.findManyIncludesSub()

    const products = await productsService.findBySubCategory(subCategoryID)

    return res.render('client/productBySubCat',{
            layout: 'main',
            categories,
            products,
            subCategory
    })
  }
}

module.exports = new ProductsController();
