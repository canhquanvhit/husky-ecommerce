const subCategoriesService = require('../services/subCategories.service')
const categoriesService = require('../services/categories.service')
class subCategoriesController {
    // GET /admin/subcategories
    async show(req, res) {
        const subCategories = await subCategoriesService.findMany()
        res.render('admin/subCategories/list', {
            layout: 'admin/categories',
            subCategories,
        })
    }
    // GET /admin/subcategories/add
    async showAddView(req, res, message) {
        const categories = await categoriesService.findMany()

        res.render('admin/subCategories/add', {
            layout: 'admin/categories',
            categories,
            message})
    }
    // POST /admin/subcategories/add
    async createOneSubCat(req, res) {
        try {
            const data = req.body
            const { name ,category } = data
            const checking = await subCategoriesService.checkExist(name)
            if(checking) {
                res.render('admin/subCategories/add', {
                    layout: 'admin/categories',
                    nameError: true})
            }
            const isCategory = await categoriesService.findOne(category)
            if (!isCategory) {
                res.render('admin/subCategories/add', {
                    layout: 'admin/categories',
                    categoryError: true})
            }

            const subCategories = await subCategoriesService.createOne(data)
            if (subCategories) {
                res.redirect('/admin/subcategories')
            }
        } catch (error) {
            console.log(error)  
        }
    }

    // GET /admin/subCategories/:id/edit
    async showEditView(req, res, message) {
        const { id } = req.params
        const subCategory = await subCategoriesService.findOne(id)

        const categories = await categoriesService.findMany()

        res.render('admin/subCategories/edit', {
            layout: 'admin/sub',
            subCategory,
            categories,
            message})
    }

    // PUT /admin.subCategories/:id/edit
    async updateOne(req, res) {
        const updateData = req.body
        await subCategoriesService.updateOne(req.params.id, updateData)

        return res.redirect('/admin/subcategories')
    }

    // DELETE /:id/delete
    async deleteOne(req, res) {
        const { id } = req.params

        await subCategoriesService.deleteOne(id)
        
        res.redirect('/admin/categories')
        
    }
}

module.exports = new subCategoriesController();
