const adminLayout = { layout: 'admin/home' }
const productsLayout = { layout: 'admin/product' }
const loginLayout = {layout: 'admin/login'}
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {config} = require('../../config/config')
const { UserService } = require('../services/user.service')
const userService = new UserService();
class AdminSiteController {
    // home
    async index(req, res) {
        res.render('admin/home', adminLayout)
    }
    search(req, res) {
        res.send('search');
    }

    async getLoginPage(req, res) {
        res.render('admin/login', loginLayout)
    }

    async login(req, res) {
        try {
            const { email, password } = req.body

            if (!email || !password) {
                return res.render('admin/login', {
                    layout: 'admin/login',
                    error: 'Missing email or password'})
            }

            const existEmailUser = await userService.checkEmailExist(email)

            if (!existEmailUser) {
                return res.render('admin/login', {
                    layout: 'admin/login',
                    error: 'Email does not exist'})
            }

            if (!existEmailUser.isAdmin) {
                return res.render('admin/login', {
                    layout: 'admin/login',
                    error: 'No permission'})
            }

            const { password: hasPass } = existEmailUser

            const isCorrectPassword = bcrypt.compareSync(password, hasPass)

            if (!isCorrectPassword) {
                return res.render('admin/login', {
                    layout: 'admin/login',
                    error: 'Password is incorrect'})
            }

            const payload = {
                userID: existEmailUser._id,
                isAdmin: true
            }
            const accessToken = await jwt.sign(payload, config.adminSecret, { expiresIn: '30d'})


            req.session.accessToken = accessToken

            return res.redirect('/admin')

        } catch (error) {
            console.log(error)
            return res.render('admin/login', {error: 'An Error while loging in'})
        }
    }
    // products
    async getProducts(req, res) {
        res.render('admin/products/list', productsLayout)
    }

    async addProduct(req, res) {
        res.render('admin/products/add', productsLayout);
    }
}

module.exports = new AdminSiteController();
 