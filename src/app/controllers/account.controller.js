
const { UserService } = require('../services/user.service')
const { authSchema } = require('../../validations/register.validation')
const {convertToObject, getUser} = require('../../helpers/get')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const { config } = require('../../config/config')
const categoriesService = require('../services/categories.service')
const invoiceService = require('../services/invoice.service')
const invoiceDetailService = require('../services/invoiceDetail.service')
const productsDetailService = require('../services/productsDetail.service')

const userService = new UserService();

class AccountController {
  async registerUser (req, res, next) {
    res.render('client/register', {layout: 'register'})
  }

  // register user
  async reqRegisterUser (req, res) {
    try {
      const { email } = req.body
      const checking = await userService.checkEmailExist(email)
      if(checking) {
        return res.render('client/register', {
          layout: 'register',
          emailError: true
        })
      }

      const userData = {
        ...req.body,
        role: 'customer',
      }

      if(userData.role == 'admin') {
        userData.isAdmin = true
      }

      const user = await userService.createOne(userData)
      if (user) {
        return res.render('client/register', {
          layout: 'register',
          message: 'đăng ký tài khoản thành công!'})
      }
    } catch (error) {
      console.log(error)
    }
    
  }

  // login user
  async getLogin(req, res) {
    const categories = await categoriesService.findManyIncludesSub()
    res.render('client/login', {
      layout: 'login',
      categories,})
	}

  async login (req, res) {
    try {
      const { email, password } = req.body

      if (!email || !password) {
        return res.render('client/login', {error: 'Missing email or password'})
      }

      const existEmailUser = await userService.checkEmailExist(email)

      if (!existEmailUser) {
        return res.render('client/login', {error: 'Email do not exist'})
      }

      const { password: hasPass } = existEmailUser

      const isCorrectPassword = await bcrypt.compareSync(password, hasPass)

      if (!isCorrectPassword) {
        return res.render('client/login', {error: 'Password is incorrect'})
      }

      const payload = {
        userID: existEmailUser._id
      }
      const accessToken = await jwt.sign(payload, config.secret, { expiresIn: '30d'})


      req.session.accessToken = accessToken

      return res.redirect('/checkout')
    } catch (error) {
      console.log(error)
      return res.render('client/login', {error: 'An Error while loging in'})
    }
  }

  async logout (req, res, next) {
    if (req.session) {
			// delete session object
			req.session.destroy(function(err) {
				if(err) {
					return next(err);
				} else {
					return res.redirect('/');
				}
			});
		}
	}	
  // my account
  async myAccount(req,res) {
    const categories = await categoriesService.findManyIncludesSub()
    const user = convertToObject(req.user)
    const query = {user: user._id}
    const invoices = await invoiceService.findMany(query)
    console.log(invoices)
    return res.render('client/account', {
      layout: 'account',
      user,
      categories,
      invoices
    })
  }

  // my addresses
  async myAddress(req, res) {
    const categories = await categoriesService.findManyIncludesSub()
		const user = getUser(req)
    res.render('client/address', {
      layout: 'address',
      categories,
			user,})
  }

  async myOrder (req, res) {
    const id = req.params.id
    const invoice = await invoiceService.findOne(id)
    const invoiceDetails = await invoiceDetailService.findMany({invoice: id})
    for(const invoiceDetail of invoiceDetails) {
      const productDetail = await productsDetailService.findOne(invoiceDetail.productDetail._id)
      invoiceDetail.productDetail = productDetail
  }
    return res.render('client/myOrder', {
      layout: 'account',
      invoiceDetails,
      invoice,
    })
  }
}

module.exports = new AccountController()