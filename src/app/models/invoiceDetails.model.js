const mongoose = require('mongoose')
const Schema = mongoose.Schema

const invoiceDetailSchema = new Schema({
    invoice: {
        type: Schema.Types.ObjectId,
        ref: 'Invoice',
    },
    productDetail: {
        type: Schema.Types.ObjectId,
        ref: 'ProductDetail',
    },
    quantity: {
        type: Number,
        required: true,
    }
},
{ 
  timestamps: true,
})
module.exports = mongoose.model('invoiceDetail', invoiceDetailSchema)