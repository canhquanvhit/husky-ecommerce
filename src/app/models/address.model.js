const mongoose = require('mongoose')
const Schema = mongoose.Schema

const addressSchema = new Schema({
    fullName: {
        type: String,
        required: true,
    },
    phoneNumber: {
      type: String,
      required: true,
    },
    province: {
        type: String,
        required: true,
    },
    district: {
        type: String,
        required: true,
    },
    subDistrict: {
      type: String,
      required: true,
    },
    addressDetail: {
        type: String,
        required: true,
    },
},
{ 
  timestamps: true,
})
module.exports = mongoose.model('address', addressSchema)