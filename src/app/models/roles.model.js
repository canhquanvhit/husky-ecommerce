import { Schema } from 'mongoose'
import { model } from './categories.model'


const rolesSchema = new Schema({
    roleName: {
        type: String,
        enum: ['customer', 'admin', 'staff'],
        required: true,
    }
})

export default model('Roles', rolesSchema)