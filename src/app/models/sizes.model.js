const mongoose = require('mongoose')
const Schema = mongoose.Schema

const sizesModel = new Schema({
    name: {
        type: String,
        required: true,
    }
})
module.exports = mongoose.model('Size', sizesModel)