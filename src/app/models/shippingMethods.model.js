
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const shippingMethodSchema = new Schema({
  transporter: {
    type : String,
    required: true,
  },
  feeShip: {
    type: Number,
    required: true,
  }
})
module.exports = mongoose.model('shippingMethods', shippingMethodSchema)
