
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productDetailSchema = new Schema({
    product: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
    },
    size: {
        type: Schema.Types.ObjectId,
        ref: 'Size',
    },
    color: {
        type: Schema.Types.ObjectId,
        ref: 'Color',
    },
    inStock: {
        type: Number,
        required: true,
    }
},
{ 
  timestamps: true,
})
module.exports = mongoose.model('ProductDetail', productDetailSchema)