const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema({
    subCategory: {
        type: Schema.Types.ObjectId,
        ref: 'SubCategory',
    },
    name: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    description: {
        type: String,
    },
    avatar: {
        type: String,
        required: true,
    },
    moreImages: [{
        type: String,
    }],
},
{ 
  timestamps: true,
})
module.exports = mongoose.model('Product', productSchema)
