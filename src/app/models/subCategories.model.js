const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SubCategorySchema = new Schema({
        name: { type: String, required: true, trim: true },
        description: { type: String },
        category: { type: Schema.Types.ObjectId,
                    ref: 'Category',
                    required: true },
    },
    { 
        timestamps: true 
    }
)



module.exports = mongoose.model('SubCategory', SubCategorySchema)