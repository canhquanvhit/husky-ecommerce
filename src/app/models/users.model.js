const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const Schema = mongoose.Schema

const userSchema = new Schema({
    lastName: {
      type: String,
      required: true,
    },
    firstName: {
      type: String,
      required: true,
    },
    gender: {
      type: Boolean,
      required: true,
    },
    birthday: {
      type: Date,
    },
    profilePhoto: {
      type: String,
    },
    email: {
        type: String,
        required: [true, 'Email is required!'],
        index: true,
        // validate : {
        //     validator : (v) => {
        //       return config.regex.email.test(v)
        //     } ,
        //     message : 'invalid email, email must be example@gmail.com'
        //   }
    },
    password: {
      type: String,
    },
    defaultAddress: {
      required: false,
      type: Schema.Types.ObjectId,
      ref: 'Address',
    },
    role:
      {
        type : Schema.Types.String,
        ref: 'roles',
      },
    isAdmin: {
      type: String,
      default: false,
    },
    isDeleted: {
      type: Boolean,
      default: false
    },
},
{ 
  timestamps: true,
})
userSchema.pre('save', async function(next) {
  if(!this.isModified('password')) {
    next()
  }
  this.password = await bcrypt.hash(this.password, 10)
  next()

})

userSchema.methods.checkPassword = async (password) => {
  try {
    const result = await bcrypt.compare(password, this.password)
    return result
  } catch (error) {
    console.log(error)
  }
}

module.exports = mongoose.model('User', userSchema)