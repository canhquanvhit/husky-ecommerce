const mongoose = require('mongoose')
const Schema = mongoose.Schema

const invoicesSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    paymentMethod: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        default: 'pending',
    },
    shippingMethod: {
        type: Schema.Types.ObjectId,
        ref: 'shippingMethod',
    },
    shipmentInfo: {
        type: String,
    },
    totalAmount: {
        type: String,
        required: true,
    }
},
{
    timestamps: true,
})
module.exports = mongoose.model('invoices', invoicesSchema)