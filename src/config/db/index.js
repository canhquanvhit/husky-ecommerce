const mongoose = require('mongoose')

const connect = async () => {
    try {
        await mongoose.connect('mongodb://localhost:27017/husky_Ecommerce', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
            })
        console.log('connect successfully!')
    } catch (error) {
        console.log('error')
    }
}

module.exports = { connect }