const config = {
    port : 9000,
    file : {
      sizeLimit : 1024*1024*30,
    },
    db : {
      host : 'mongodb://localhost:27017/husky_Ecommerce',
      options : {
        useNewUrlParser: true, 
      }
    },
    secret : "KADnaskOSSDNXSLS@*&^%$121321KSAKNCFISAKLASMANDAKksas154545#@%kKQPEOXMNA",
    adminSecret: "asrioKLHUY(OIpLKSJDJWICJNWL(&*#&@(@)_@)-934232jAKJKDJKkkhu",
    regex : {
      email : /\S+@\S+\.\S+/,
      // numberPhone : /\.[0,9]/
    },
    admin : {
      email : process.env.ADMIN_EMAIL,
      password : process.env.ADMIN_PASS
    },
    student : {
      resetPassword : '123123123'
    },
    mailer : {
      email : process.env.MAIL_ADD,
      pass : process.env.MAIL_PASS
    }
}

module.exports = { config }