const express = require('express');
const app = express();
const port = 3000;
const morgan = require('morgan');
const handlebars = require('express-handlebars');
const path = require('path');
const bodyParser = require('body-parser')
const route = require('./routes/index.router');
const session = require('express-session')
const db = require('./config/db')
const passport = require('passport')
require('../src/utils/localStrategy')
const methodOverride = require('method-override')
// connect to db
db.connect()

app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride('_method'))
// HTTP logger
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({ extended: false }))

// template engine

app.engine(
    'hbs',
    handlebars({
        extname: 'hbs',
        helpers: {
            sum: function (a, b) { return a + b },
            switch: function(value, options) {
                this.switch_value = value
                return options.fn(this)
                },
            case: function(value, options) {
                if (value == this.switch_value) {
                  return options.fn(this)
                }},
            currencyFormat: function(number) {
                return new Intl.NumberFormat('vi', {
                    style: 'currency',
                    currency: 'VND',
                }).format(number)
            },
            multiple: function (a,b) { return a * b},
    }}),

)

app.set('view engine', 'hbs');
app.use(session({
    secret: 'e6ef250e680362c45f21b8417fae966ccbf751e0',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  }))

app.use(passport.initialize())
app.use(passport.session())

app.set('views', path.join(__dirname, 'resources','views'))

// routes init
route(app)

app.listen(port, () => console.log(path.join(__dirname,"adava"), port ,'dasdasdasdasdasd'));
