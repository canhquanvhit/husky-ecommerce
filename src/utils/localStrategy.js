const User = require('../app/models/users.model')
const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy({
    usernameField: 'email'
  },
  async (email, password, roles, done) => {
      try {
        const user = await User.findOne({ email }) 

        if(!user) 
          return done(null, false)
        
        if(await user.checkPassword(password))
          return done(null, user)
        
        return done(null, false)
      } catch (error) {
          console.log(error)
      }
  }
));


passport.serializeUser((user, done) => {
    done(null, user._id);
  });
  
  passport.deserializeUser(async (_id, done) => {
    try {
        const user = await User.findOne({ _id })
        done(null, user)
        
    } catch (error) {
        done(error)
    }
  });