const jwt = require('jsonwebtoken')
const {config} = require('../config/config')
const { UserService } = require('../app/services/user.service')
const userService = new UserService()

const checkUserLogged = async (req, res, next) => {
	const accessToken = req.session.accessToken
  if(!accessToken) {
    return next()
  }
  let payload
  try {
    payload = await jwt.verify(accessToken, config.secret)
  } catch (error) {
    
  }

  if(!payload) {
    return next()
  }
	req.user = await userService.findOne({ _id: payload?.userID })
	
	next()
}

module.exports = checkUserLogged 