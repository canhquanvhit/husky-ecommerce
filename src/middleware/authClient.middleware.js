const jwt = require('jsonwebtoken')
const {config} = require('../config/config')
const { UserService } = require('../app/services/user.service')
const userService = new UserService()

const authClientMiddleware = async (req, res, next) => {
	const accessToken = req.session.accessToken

	console.log(accessToken)

	if (!accessToken) {
		return res.redirect('/account/login')
	}

	const payload = await jwt.verify(accessToken, config.secret)

	if(!payload) {
		return res.redirect('/account/login')
	}

	req.user = await userService.findOne({ _id: payload.userID })
	
	next()
}

module.exports = authClientMiddleware