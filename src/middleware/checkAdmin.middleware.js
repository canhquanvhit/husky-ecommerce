const jwt = require('jsonwebtoken')
const {config} = require('../config/config')
const {UserService} = require('../app/services/user.service')
const userService = new UserService()

const checkAdminMiddleware = async (req, res, next) => {
	const accessToken = req.session.accessToken

	console.log(accessToken)

	if (!accessToken) {
		return res.redirect('/admin/login')
	}
	let payload
	try {
		payload = await jwt.verify(accessToken, config.adminSecret)
	} catch (error) {
		payload = await jwt.verify(accessToken, config.secret)
	}
	

	if(!payload) {
		return res.redirect('/admin/login')
	}

  const user = await userService.findOne({ _id: payload.userID })
  
  if (!user.isAdmin) {
    return res.redirect('/admin/login')
  }

  req.user = user
	
	next()
}

module.exports = {checkAdminMiddleware}