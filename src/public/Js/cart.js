var keyLocalStorage = 'products';
function createItemInCart(product) {
    var itemCart = {};
    itemCart = product;
    return itemCart;
}

function getAllItemCart() {
    var itemsCart = [];
    // get Json in localStorage
    var jsonLocalStorage = localStorage.getItem(keyLocalStorage);
    if (jsonLocalStorage != null) {
        itemsCart = JSON.parse(jsonLocalStorage);
    }
    return itemsCart;
}
function setAllItemToLocalStorage(listItemCart) {
    var jsonListItemCart = JSON.stringify(listItemCart);
    localStorage.setItem(keyLocalStorage, jsonListItemCart);
}

var listItemCart = getAllItemCart();
//delete

function showSiteCartListItem() {
    var getCartListItem = getAllItemCart();
    var HTML = convertCartListItemToSiteCartHTMLText(getCartListItem);
    var nodeSiteCart = $('#cart-view');
    var totalAmountNode = $('.total-price b');
    nodeSiteCart.html(HTML);

    totalAmountNode.text(1312);

    countItemInSiteCart();
}

function deleteItemInCart(productId, color, size) {
    var listItemCart = getAllItemCart();
    console.log(productId + color + size);
    if (color == null && size == null) {
        for (let i = 0; i < listItemCart.length; i++) {
            if (listItemCart[i].productId == productId) {
                listItemCart.splice(i, 1);
            }
        }
    } else {
        for (let i = 0; i < listItemCart.length; i++) {
            if (
                listItemCart[i].productId == productId &&
                listItemCart[i].productColor == color &&
                listItemCart[i].productSize == size
            ) {
                listItemCart.splice(i, 1);
            }
        }
    }

    setAllItemToLocalStorage(listItemCart);
    // showCartListItem();
    showSiteCartListItem();

}
//

// cal total amount
function calTotalAmount(listItemCart) {
    var totalAmount = 0;
    for (let i = 0; i < listItemCart.length; i++) {
        totalAmount += listItemCart[i].amount * listItemCart[i].productPrice;
    }
    return {
        text: convertFormatPrice(totalAmount),
        number: totalAmount,
    }
}


// Count items in the cart

function countItemInCart() {
    amountItem = $('.cart-item').length;
    if (amountItem == 0) {
        $('#cart-control-checkout').hide();
        $('#cart-control-update').hide();
    } else {
        $('#cart-control-checkout').show();
        $('#cart-control-update').show();
    }
    $('.count-cart span').text(amountItem);
}

