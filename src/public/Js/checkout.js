$(document).ready(() => {
  $.support.cors = true

  $.ajax({
    url: `http://localhost:3000/provinceAPIs/provinces`,
    method: 'GET',
    success: function(data){
      const listProvinces = data.LtsItem.map(each => {
        return `<option id="${each.ID}" value="${each.Title}">${each.Title}</option>`
      });

      $('#customer_shipping_province').html(listProvinces.join(''))

      $.ajax({
        url: `http://localhost:3000/provinceAPIs/provinces/${data.LtsItem[0].ID}/district`,
        method: 'GET',
        success: function(data){
          const listProvinces = data.map(each => {
            return `<option id="${each.ID}" value="${each.Title}">${each.Title}</option>`
          });

          $.ajax({
            url: `http://localhost:3000/provinceAPIs/district/${data[0].ID}/ward`,
            method: 'GET',
            success: function(data1){
              const listProvinces1 = data1.map(each => {
                return `<option  id ="${each.ID}" value="${each.Title}">${each.Title}</option>`
              });
        
              $('#customer_shipping_ward').html(listProvinces1.join(''))
        
        
            },
            error: function(error) {
              console.log(error)
            }
          })
        
    
          $('#customer_shipping_district').html(listProvinces.join(''))
        },
        error: function(error) {
          console.log(error)
        }
      })
    },
    error: function(error) {
      console.log(error)
    }
  })

  $('#customer_shipping_province').on('change',(e) => {
    var id = $('#customer_shipping_province option:selected').attr('id');

    $.ajax({
      url: `http://localhost:3000/provinceAPIs/provinces/${id}/district`,
      method: 'GET',
      success: function(data){
        const listProvinces = data.map(each => {
          return `<option id="${each.ID}" value="${each.Title}">${each.Title}</option>`
        });
  
        $('#customer_shipping_district').html(listProvinces.join(''))

        $.ajax({
          url: `http://localhost:3000/provinceAPIs/district/${data[0].ID}/ward`,
          method: 'GET',
          success: function(data1){
            const listProvinces1 = data1.map(each => {
              return `<option value="${each.Title}">${each.Title}</option>`
            });
      
            $('#customer_shipping_ward').html(listProvinces1.join(''))
      
      
          },
          error: function(error) {
            console.log(error)
          }
        })
      },
      error: function(error) {
        console.log(error)
      }
    })
  })

  $('#customer_shipping_district').on('change',(e) => {
    var id = $('#customer_shipping_district option:selected').attr('id');

    $.ajax({
      url: `http://localhost:3000/provinceAPIs/district/${id}/ward`,
      method: 'GET',
      success: function(data){
        const listProvinces = data.map(each => {
          return `<option id="${each.ID}" value="${each.Title}">${each.Title}</option>`
        });
  
        $('#customer_shipping_ward').html(listProvinces.join(''))
  
  
      },
      error: function(error) {
        console.log(error)
      }
    })
  })

  let totalAmount

  showCheckoutListProduct()

  function showCheckoutListProduct () {
    var getCartListItem = getAllItemCart();
    var HTML = convertCartListItemToCheckoutHTMLText(getCartListItem)
  
    var nodeCheckoutProduct = $('#list_product-variant')
    nodeCheckoutProduct.html(HTML)
    
    var totalAmountTempoNode = $('#total-tempo')

    var totalAmountNode = $('#payment-due-price')

    var shippingFeeNode = $('#shipping-fee')

    var shippingFee = shippingFeeNode.attr("value")

    totalAmount = Number(shippingFee) + calTotalAmount(getCartListItem).number

    totalAmountNode.attr('value', totalAmount)
    totalAmountNode.text(convertFormatPrice(totalAmount))
    totalAmountTempoNode.text(calTotalAmount(getCartListItem).text)
  }
  
  function convertCartListItemToCheckoutHTMLText(listItemCart) {

    var HTMLTotal = ''
    for(let i = 0 ; i < listItemCart.length; i ++) {
      HTMLTotal += convertCartItemToCheckoutHTML(listItemCart[i])
  }

  return HTMLTotal;
  }
  
  function convertCartItemToCheckoutHTML(cartItem) {
    const productDetail = { // đây là lấy mỗi object - mỗi thằng product
      color: cartItem.productColor?.id,
      size: cartItem.productSize?.id,
      amount: cartItem.amount,
    }
    var html ='<tr class="product" data-product-id="'+ cartItem.productId + '" data-variant-id="1065075526">'+
              '<td class="product-image">'+
              '  <div class="product-thumbnail">'+
              '    <div class="product-thumbnail-wrapper">'+
              '      <img class="product-thumbnail-image" alt="'+cartItem.productName+'" src="'+cartItem.ProductImagePath+'">'+
              '    </div>'+
              '    <span class="product-thumbnail-quantity" aria-hidden="true">'+cartItem.amount+'</span>'+
              '  </div>'+
              '</td>'+
              '<td class="product-description">'+
              '  <span class="product-description-name order-summary-emphasis">'+cartItem.productName+'</span>'+
              `<input class="product-detail" name="productDetail" value="${JSON.stringify(productDetail).replace(/"/g, `'`)}" type="hidden">`+
              '    <span class="product-description-variant order-summary-small-text">'+
              '      '+cartItem.productColor?.name +' / '+cartItem.productSize?.name +
              '    </span>'+
              '</td>'+
              '<td class="product-price">'+
              '  <span class="order-summary-emphasis">'+convertFormatPrice(parseInt(cartItem.productPrice)*cartItem.amount)+'</span>'+
              '</td>'+
              '</tr>';

    return html
  }
  
  const productDetailNodes = $('.product-detail')

  const productDetails = []
  for (var productDetailNode of productDetailNodes) {
    productDetails.push(productDetailNode.defaultValue)
    // này là lấy ra list product đó 
  }

  const checkoutBtn = $('#btn-submit-checkout')

  const formCheckout = $('#form_next_step')

  checkoutBtn.on("click", formCheckout.submit(function(eventObj) {
      $(this).append($('#billing_address_full_name')[0])
      $(this).append($('#billing_address_phone')[0])
      $(this).append($('#billing_address_address-detail')[0])
      $(this).append($('#customer_shipping_province')[0])
      $(this).append($('#customer_shipping_district')[0])
      $(this).append($('#customer_shipping_ward')[0])
      $(this).append($('#shipping_method')[0])
      $(this).append($('#payment_method')[0])
      $(this).append('<input class="product-detail" name="productDetails" value="['+productDetails+']" type="hidden">')
      $(this).append('<input name="totalAmount" value="'+totalAmount+'" type="hidden">')

      return true
    })

  ) 

 

})

