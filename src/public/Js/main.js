function goBack() {
    window.history.back();
}
function isUndefined(str) {
    return typeof str === 'undefined';
}
$('#logo-fixed').hide();
$(document).ready(function () {
    console.log('hello')
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 20,
        nav: true,
        responsive: {
            0: {
                items: 2,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 5,
            },
        },
    });
    var width = 0;
    $('#header-cart').click(function () {
        $('#site-overlay').addClass('active');
        $('#main-body').addClass('sidebar-move');
        $('.site-nav').addClass('active show-cart');
    });

    $('#header-search').click(function () {
        $('#site-overlay').addClass('active');
        $('#main-body').addClass('sidebar-move');
        $('.site-nav').addClass('active show-search');
    });

    $('#header-nav-mobile').click(function () {
        $('#site-overlay').addClass('active');
        $('#main-body').addClass('sidebar-move');
        $('.site-nav').addClass('active show-nav-mobile');
    });

    $('.site-close-handle').click(function () {
        $('#site-overlay').removeClass('active');
        $('#main-body').removeClass('sidebar-move');
        $('.site-nav').removeClass('active show-cart show-search');
    });

    window.onscroll = function () {
        scrollFunction();
    };

    function scrollFunction() {
        if (
            document.body.scrollTop > 100 ||
            document.documentElement.scrollTop > 100
        ) {
            $('#main-header').addClass('fixed-view');
            $('.topbar-content').addClass('fixed-view');
            $('#logo-fixed').show();
        } else {
            $('#main-header').removeClass('fixed-view');
            $('.topbar-content').removeClass('fixed-view');
            $('#logo-fixed').hide();
        }
    }

    //sub-mobile-appear

    listCate = $('.icon-has-sub');
    for (var i = 0; i < listCate.length; i++) {
        $(listCate[i]).on('click', function (event) {
            if (!$(this).parent().hasClass('active')) {
                $(this).parent().addClass('active');
            } else $(this).parent().removeClass('active');
            // $(this).parent().addClass('active');
        });
    }

    // convert number to cunrency
});

function convertFormatPrice(number) {
    return new Intl.NumberFormat('vi', {
        style: 'currency',
        currency: 'VND',
    }).format(number);
}
function showResult(str) {
    if (str.length == 0) {
        $('.result-content').html('');
        $('.result-content').hide();
        return;
    }
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            $('.result-content').show();
            $('.result-content').html(this.responseText);
        }
    };
    xmlhttp.open(
        'GET',
        '../323goods/classes/BL/liveSearch.php?key=' + str,
        true,
    );
    xmlhttp.send();
}
